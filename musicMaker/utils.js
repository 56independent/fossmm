// ChatGPT generated the bodies of these functions
function putInURL(arg, data) {
  var currentUrl = new URL(window.location.href);
  var params = new URLSearchParams(currentUrl.search);
  params.delete(arg);

  params.append(arg, data);

  var newUrl = currentUrl.origin + currentUrl.pathname + '?' + params.toString();
  window.history.replaceState(null, null, newUrl);
}

function getFromURL(arg) {
  const urlParams = new URLSearchParams(window.location.search);
  var data = urlParams.get(arg) || "";

  return data
}

function deleteArgFromURL(arg) {
  var currentUrl = new URL(window.location.href);
  var params = new URLSearchParams(currentUrl.search);
  params.delete(arg);
}

// ChatGPT made this code
function downloadText(text, filename, type="text/plain") {
  const blob = new Blob([text], { type: type});

  // Create a download link element
  const link = document.createElement('a');
  link.href = URL.createObjectURL(blob);
  link.download = filename;

  // Append the link to the document and trigger the download
  document.body.appendChild(link);
  link.click();

  // Clean up by removing the link element
  document.body.removeChild(link);


}

function openFileDialogAndGetContents() {
  // Create a hidden file input element
  const fileInput = $('<input type="file" style="display: none;">');

  // Append the file input to the body
  $('body').append(fileInput);

  // Trigger a click on the hidden file input
  fileInput.click();

  // Return a Promise to handle asynchronous file reading
  return new Promise((resolve, reject) => {
      // Attach a change event to the file input
      fileInput.on('change', function(event) {
          // Retrieve the selected file
          const selectedFile = event.target.files[0];

          // Read the file contents
          const reader = new FileReader();
          reader.onload = function(e) {
              // Remove the file input from the DOM
              fileInput.remove();

              // Resolve the Promise with the file contents as a string
              resolve(e.target.result);
          };
          reader.onerror = function() {
              // Remove the file input from the DOM
              fileInput.remove();

              // Reject the Promise on error
              reject(new Error('Error reading file'));
          };
          reader.readAsText(selectedFile);
      });
  });
}
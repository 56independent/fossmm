function actuallyRunFunction(functionName, data) {
    let returnValue = 0

    switch (functionName) {
        case "+":
            for (dataElement of data) {
                if (dataElement !== undefined){
                    returnValue += dataElement-1+1
                }
            } 
            break

        case "++":
        case "*":
            for (dataElement of data) {
                returnValue =  returnValue*dataElement
            }
            break

        case "-":
            for (dataElement of data) {
                returnValue -= dataElement
            }
            break

        case "--":
        case "/":
            for (dataElement of data) {
                returnValue =  returnValue/dataElement
            }
            break
        
        case "print":
            console.log(data)
            break
        
        case "time":
            returnValue = Date.now()
            break

        default:
            console.log("Function `" + functionName + "` not defined")
    }

    return returnValue
}
/*
Plot interpreter and standard libraries.

To define a standard library, simply have a function defined like this, and include it into the JS in whichever way you use.

function actuallyRunFunction(functionName, data) {
    let returnValue

    switch (functionName) {
        case "": // The name of your function goes here
            // Code for the function goes here; assign any value to returnValue to give back a value.
            break

        default:
            console.log("Function `" + functionName + "` not defined")
    }

    return returnValue
}
*/

INTERPRETER = {
    VARIABLE_DEFINE: "define",
    FUNCTION_DEFINE: "function",
    STRING_L: ["\""], // TODO: Add more string delimiters
    STRING_R: ["\""]
}


// TODO: Implement escaped strings, other string discriminators, and a proper standard library

class stack {
    push(items, isArray) {
        if (isArray) {
            for (const item of items){
                this.stack.push(item)
            }
        } else {
            this.stack.push(items)
        }
    }

    peek(numberBack=1) {
        return this.stack[this.stack.length-numberBack]
    }

    remove(amount) {
        let returnValues = []
        for (let i = 0; i < amount; i++){
            returnValues.push(this.stack.pop())
        }

        return returnValues // Por supuesto TODO: Could this be made better?
    }

    clear() {
        this.stack = []
    }

    constructor (items=[]){
        this.stack = []
        this.push(items)
    }
}

class plotInterpreter {
    runfunction(functionName, data) { // Where data are the arguments and we have an array
        return actuallyRunFunction(functionName, data)
    }

    runlexed(lexedObj, state) {
        // We're going to be lexing a 2d array. Since RPN Plot (Płot) works well on the stack, we'll be using that.
        // To run the code, we have a small "memory stack". This stack collects all the data needed to run a function.
        // After having been run, the return value, if any, becomes the first bit of data in the stack.

        let memory = new stack()

        // Oh and also, let's start an object to hold any data we define:

        state = state || [["variable", "test", "HI"]] // Datatype, name, and value

        // In the case of functions, the value is equivalent to the RPN of the function.

        console.log(lexedObj)

        function inState(state, name, type) {
            name = name.replace(/"/g, '');
            for (const value of state) {
                //console.log(value[1], name)
                if (value[0] == type && value[1] == name){return true}
            }

            return false
        }

        for (const thing of lexedObj) {
            if (thing[0] != "function" && thing[0] != "seperator") {
                memory.push(thing[1])
            } else {
                //console.log(thing, memory.stack)

                if (inState(state, thing[1], "variable")) {
                    // Find value of the variable
                    let contents = ""
                    for (const value of state) {
                        //console.log(value)
                        if (value[1].replace(/"/g, "") == thing[1].replace(/"/g, "")){
                            contents = value[2]
                            break
                        }
                    }
                    //console.log(thing[1], contents)

                    memory.push(contents)
                } else if (inState(state, thing[1], "function")){
                    // TODO: Implement functions
                } else if (thing[1] == INTERPRETER.FUNCTION_DEFINE) {
                    //console.log(memory.state)
                    state.push(["function", memory.peek(1), this.lexTokens(this.RPNisePwot(memory.peek(2)))]) // Take the function definition, which will be a string, and convert it to lexed elements
                    memory.remove(2)
                } else if (thing[1] == INTERPRETER.VARIABLE_DEFINE) {
                    state.push(["variable", memory.peek(1), memory.peek(2)]) // TODO: Expand to allow lists, too
                    memory.remove(2)
                } else if (thing[0] == "seperator"){
                    memory.clear()
                } else {
                    let returnValue = this.runfunction(thing[1], memory.stack)
                    memory.clear()
                    if (returnValue !== '') {
                        memory.push(returnValue);
                    }
                }
            }
        }

        this.innards = memory.stack
        return memory.stack
    }

    lexTokens(tokens) {
        // Ok, so we now have our tokens.
        // In plot, there are only three datatypes, by priority:
            // The string; denoted by "", (unsupported) '', or (unsupported) «».
            // The number; denoted by matching the regex \d+ or \d+\.\d+
            // The function; These do not match either of the above and are the things that run on the previous objects
        // As such, we can represent plot code as a 2D array. The first column is the datatype and the second the data.

        let lexed = new stack()

        for (const token of tokens){
            if (token != "") {
                let dataThing = []
                if (/".*"/.test(token)) {
                    dataThing = ["string", token.replace(/"/g, "")]
                } else if (/\d+\.\d+/.test(token) || /\d+$/.test(token)) {
                    dataThing = ["number", token]
                } else if (/\|/.test(token)){
                    dataThing = ["seperator", token]
                } else {
                    dataThing = ["function", token]
                }

                lexed.push(dataThing)
            }
        }

        //console.log(lexed.stack)
        return lexed.stack
    }

    tokeniseRPN(string) {
        // To tokenise, we simply split at any spaces at the non-string level.
        let inString = false
        let block = ""

        let ourCode = new stack()

        for (const char of string) {
            if (char == "\"") { // TODO: Support multiple types of strings.
                inString = !inString
            } 
            
            if (!inString) {
                if (char == " ") {
                    ourCode.push(block)
                    block = ""
                } else {
                    block += char
                }
            }
        }

        //console.log(ourCode.stack)

        return ourCode.stack
    }

    preprocessPłot(string) {
        return string
    }

    RPNisePwot(string) {
        // TODO: Clean me up!

        // To RPNise, we simply turn all function chains into a series of single function calls:

        // (print + (+ 2 3) + time number)
        // or even
        // (print (+ (+ 2 3) (+ time number))
        // Becomes
        //
        //    number time +   2 3 +  +  print
        //    Fd     Fd   f   d d f  f  f
        // (which can be read as:)
        // (((number time +) (2 3 +) +) print)

        // Every plot program _should_ be valid without brackets as forwards polish.

        // This turns plot from plot into a more compilable but illegible RPN verion.
        // To do this, we simply follow this algorithm:
            // Iterate backwards
            // Find the data block there
            // Add it to a stack
            // Given the stack, reconstruct it as plot

        // First though, we split all the top-lvel statements because they go in order. 
        
        function tokenise(string) {
            let codeList = new stack

            let block = ""
            let inString = false


            if (inString) {console.warn("Provided code, " + string + " seems to have an odd number of quote marks!")}

            inString = false

            //console.log(string)

            // Since string has no backets and it's in FPN, we can simply split it by space, merge all the stringly bits, and submit that as tokens.

            let code = string.split(" ")

            //console.log(code)
            for (const pseudotoken of code){
                if (pseudotoken.includes("\"")) {
                    if (inString) {
                        //console.log(block, pseudotoken)
                        inString = false
                        block += " " + pseudotoken
                        codeList.push(block)
                        block = ""
                    } else {
                        let small = pseudotoken.split("")

                        if (small[0] == "\"" && small[small.length-1] == "\"") {
                            codeList.push(pseudotoken)
                        } else {
                            inString = true
                            block += pseudotoken
                        }
                    }
                } else if (inString) {
                    block += " " + pseudotoken
                } else {
                    codeList.push(pseudotoken)
                }
            }
            
            return codeList.stack.reverse()
        }

        let RPNStack = new stack()
        let codes = this.extractTopLevelFunctionCalls(string)

        for (const thing of codes) {
            //console.log(thing)
            RPNStack.push(tokenise(thing), true)
            RPNStack.push("|")
        }

        //console.log(RPNStack.stack)

        let RPN = ""

        for (const block of RPNStack.stack) {
            RPN += " " + block
        }

        console.log(RPN)
        return [RPN, RPNStack.stack] // So the tokens are also given in case we're passing them to a function.
    }

    // https://chat.openai.com/share/9de6aa03-fa69-4f19-9a0b-8e23fb4ef376 made this function
    extractTopLevelFunctionCalls(input) {
        let topLevelCalls = [];
        let stack = [];
        let currentCall = "";
      
        for (let char of input) {
          if (char === "(") {
            stack.push("(");
            if (stack.length === 1) {
              if (currentCall.trim() !== "") {
                topLevelCalls.push(currentCall.trim());
              }
              currentCall = "";
            }
          } else if (char === ")") {
            stack.pop();
            if (stack.length === 0) {
              if (currentCall.trim() !== "") {
                topLevelCalls.push(currentCall.trim());
              }
              currentCall = "";
            }
          } else {
            currentCall += char;
          }
        }
      
        return topLevelCalls;
    }      

    preProcessPwot(string) {
        // I don't know what to pre-process lol

        return string
    }

    interpret(string, languageType) {
        this.useRPN = (languageType == "pwot" || languageType == "plisp") ? false : true
        this.code = string

        this.tokens = []

        if (! this.useRPN) {
            this.tokens = this.RPNisePwot(this.preProcessPwot(string))[1]
        } else {
            this.tokens = this.tokeniseRPN(string)
        }

        return this.runlexed(this.lexTokens(this.tokens))
    }

    constructor (text, languageType, test) {
        if (test || !text) {
            text = "(define \"number\" 3)(print  + (+ 2 3)  +  5  number)(print time)(print \"Hello, World!\")"
        }

       this.final = this.interpret(text, languageType)
    }
}

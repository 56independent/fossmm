class arranger { // TODO: Remove commented code and refactor
    placeTable(tracks, parent, mainObj, [[obj], parent2]) {
        function generateTrackTemplate(idPrefix, length=500){
            let row = $("<td>")

            console.log(row)
            for (let i = 0; i < length; i++){
                row.append(
                    $("<td>").attr("id", idPrefix + i).attr("class", "normalSegment").click(() => { // TODO: make a new interface for writing new notes

                    }).text("no")
                    
                    // $("<button>").css({"backgroundColor": "lime"}).text("Arghh").click(() => {
                    //     let piano = new pianoRoll([trackID, i, mainObj], $("#pianoROll"))
                    //     console.log(piano)
                    // }).attr("id", idPrefix + i)
                )
            }

            return row
        }

        function generateRollViews(track, idPrefix, [trackID, mainObj]) {
            // To make the rolls show up, we can simply use a table. There are two main types of cell; the occupied and the unnocupied. Unnocupied cells show with deafult styling. Occupied cells show green and can be clicked to open their piano roll.
            // Ok, so to make the thing we simply take the length of the given track and give them all table cells with an id representing their position and track name
            // From there, given the track, we can iterate over the rolls and fill things in as needed

            for (let i = 0; i < track.rolls.length; i++) {
                // = track.rolls[i]
                const start = track.start[i]
                let id = "#" + idPrefix + start

                console.log(id, $(id))
                
                $(id).attr("class", "pianoRollSegment").click(() => {
                    let thing = new pianoRoll([trackID, i, mainObj], parent2) // TODO: Name this variable better
                }).text("piano")
                
                console.log(id, $(id))

            }
        }

        let table = $("<table>")

        table.append($("<tr>").append(
            $("<td>").append($("<button>").text("Save").click(() => {
                console.log(mainObj.project)

                let saveablecopy = mainObj.project

                // Remove synth from each of the lines as it causes problems

                for (let i = 0; i < saveablecopy.lines.length; i ++) {
                    saveablecopy.lines[i].synth = null
                }

                downloadText(JSON.stringify(saveablecopy, null, 4), "FOSS MM.json")
            })),
            $("<td>").append($("<button>").text("Load").click(() => {
                // ChatGPT generated this code
                openFileDialogAndGetContents().then(contents => {
                    mainObj.project = contents
                }).catch(error => {
                    console.error(error);
                });
            })),
            $("<td>").append($("<button>").text("Play").click(() => { // TODO: implement main player pause and play buttons
                // Add code here
            })),
            $("<td>").append($("<button>").text("Plugins").click(() => {
                let pluginManager = new plugins(pageLay.addSplit("down"), main)
                //prompt("Nope! It's not gunna work yet!")
            })),
            $("<td>").append($("<button>").text("New track").click(() => { // TODO: Could we use a less destructive method?
                let beeper = new synth("test")

                mainObj.project.lines.push({
                    synth: beeper.synthThing,
                    name: "TestSynth",
                    solo: false,
                    mute: true,
                    start: [0], // This is shit, but it's the start of each roll. It's the simplest to implement with the hole i got myself into. TODO: refactor this shit away
                    rolls:[
                        [
                            {
                                "note":"C4",
                                "start":0,
                                "end":2,
                                "bend":0,
                                "velocity":50,
                            },
                        ]
                    ]
                })

                parent.children().remove()
                this.placeTable(tracks, parent, mainObj, [[obj], parent2])
            })),
            
            // $("<td>").append($("<button>").text("URL-ise").click(() => {
            //     console.log(mainObj.project)

            //     let saveablecopy = mainObj.project

            //     // Remove synth from each of the lines as it causes problems with infite recursion 

            //     for (let i = 0; i < saveablecopy.lines.length; i ++) {
            //         saveablecopy.lines[i].synth = null
            //     }

            //     let projectString = JSON.stringify(saveablecopy)

            //     putInURL("project", LZipper.compress(projectString))
            // })),
            // $("<td>").append($("<button>").text("Get from URL").click(() => {
            //     let compressed = getFromURL("project")
            //     console.log(compressed)
            //     let projectString = LZipper.decompress(compressed)
            //     console.log(projectString)
            //     let thing = new storage(false, JSON.parse(projectString)) // TODO: What on earth is this
            // }))

        ))

        for (let i = 0; i < tracks.length; i++) {
            let track = tracks[i]

            // `Settings | Synth icon | Name | Vol | Pan | FX channel | Mute/Solo | Tracks`
            table.append(
                $("<tr>").attr("id", "rollfor" + i).append(
                    $("<td>").append($("<button>").text("settings")),
                    $("<td>").append(), // TODO: Add synth icon
                    $("<td>").append(track.name),
                    $("<td>").append(
                        $('<input type="text" data-skin="tron" value="25" class="dail" id="' + track.name + '-volume-dial">').knob({
                            'change' : function (v) {mainObj.project.lines[i].volume = v}
                        })
                    ).css("height", "1vh"),
                    $("<td>").append(
                        $('<input type="text" data-skin="tron" value="50" class="dail" id="' + track.name + '-panning-dial">').knob({
                            'change' : function (v) {mainObj.project.lines[i].panning = v}
                        })
                    ).css("height", "1vh"),
                    $("<td>").append(
                        $('<input type="text" value="0" id="' + track.name + '-fx-channel">').change(function (v) {mainObj.fxChan = v}).attr('style', 'width: 2em;')
                    ),
                    $("<td>").append(
                        $("<button>").text("S").click(() => {mainObj.project.lines[i].solo = ! mainObj.project.lines[i].solo}),
                        $("<button>").text("M").click(() => {mainObj.project.lines[i].mute = ! mainObj.project.lines[i].mute})
                    ),
                    generateTrackTemplate("tracks-" + i, 500) // TODO: replace static length with proper length
                )
            )
                
                        
            $(document).ready(function() {
                generateRollViews(track, "tracks-" + i , [i, mainObj])
            })
        }

        parent.append(table)
    }

    constructor(mainObj, parent, [[obj], parent2]) {
        this.placeTable(mainObj.project.lines, parent, mainObj, [[obj], parent2])
        this.trackRow = 7
    }
}
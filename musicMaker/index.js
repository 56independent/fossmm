class storage {
    constructor(empty, data=NaN) {
        if (!empty){
            let beeper = new synth("test")

            this.project = {
                settings:{
                    "bpm":120,
                    "sig":4,
                    "plugins":[
                        {
                            pluginName: "Test",
                            description: "Tiny plugin",
                            author: "",
                            language: "",
                            repo: "",
                            code: "(print \"Hello, World!\")"
                        }
                    ]
                },
                lines:[
                    {
                        synth: beeper.synthThing,
                        name: "TestSynth",
                        solo: false,
                        mute: true,
                        start: [0, 200], // This is shit, but it's the start of each roll. It's the simplest to implement with the hole i got myself into. TODO: refactor this shit away
                        rolls:[
                            [
                                {
                                    "note":"C4",
                                    "start":0,
                                    "end":2,
                                    "bend":0,
                                    "velocity":50,
                                },
                                {
                                    "note":"D#4",
                                    "start":2,
                                    "end":4,
                                    "bend":0,
                                    "velocity":50,
                                },
                                {
                                    "note":"E4",
                                    "start":4,
                                    "end":8,
                                    "bend":0,
                                    "velocity":50,
                                },
                                {
                                    "note":"D4",
                                    "start":8,
                                    "end":10,
                                    "bend":0,
                                    "velocity":50,
                                },
                                {
                                    "note":"B4",
                                    "start":8,
                                    "end":10,
                                    "bend":0,
                                    "velocity":50,
                                },
                                {
                                    "note":"C4",
                                    "start":10,
                                    "end":12,
                                    "bend":0,
                                    "velocity":50,
                                },
                                {
                                    "note":"E4",
                                    "start":10,
                                    "end":12,
                                    "bend":0,
                                    "velocity":50,
                                }
                            ],
                            [
                                {
                                    "note":"C4",
                                    "start":0,
                                    "end":2,
                                    "bend":0,
                                    "velocity":50,
                                },
                                {
                                    "note":"D#4",
                                    "start":2,
                                    "end":4,
                                    "bend":0,
                                    "velocity":50,
                                },
                                {
                                    "note":"E4",
                                    "start":4,
                                    "end":8,
                                    "bend":0,
                                    "velocity":50,
                                },
                            ]
                        ]
                    }
                ]
            }
        } else {
            this.project = data
        }
    }

    writeLineMeta(line, data){
        this.project.lines[line].synth = data.synth
    }

    writeRoll(line, roll, data){
        this.project.lines[line].rolls[roll] = data
    }

    getRoll (line, roll){
        return this.project.lines[line].rolls[roll]
    }

    giveContext(line) {
        return this.project.lines[line].synth
    }

    addPlugin(pluginObj){
        this.project.settings.plugins.push(pluginObj)
    }

    updatePlugin(pluginObj, name){
        for (let i = 0; i < this.project.settings.plugins.length; i++){
            if (this.project.settings.plugins[i].pluginName == name) {
                this.project.settings.plugins[i] = pluginObj
                return true
            }
        }

        return false
    }

    getPlugin(name){
        for (let i = 0; i < this.project.settings.plugins.length; i++){
            if (this.project.settings.plugins[i].pluginName == name) {
                return this.project.settings.plugins[i]
            }
        }

        return false
    }
}

main = new storage(false)
pageLay = new pageLayout()

tracks = new arranger(main, pageLay.addSplit("down", "arranger"), [[main], pageLay.addSplit("down", "pianoROll")])
piano = new pianoRoll([0, 0, main], pageLay.addSplit("down", "pianoRoll"))
//pluginManager = new plugins(pageLay.addSplit("down", ), main)

$("button").attr("class", "btn btn-primary")

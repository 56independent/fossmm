function Layout(k){
    switch(k.id){
    case "xrange":
        document.getElementById("roll-roll").xrange=k.value*timebase;
        break;
    case "xoffset":
        document.getElementById("roll-roll").xoffset=k.value*timebase;
        break;
    case "yrange":
        document.getElementById("roll-roll").yrange=k.value;
        break;
    case "yoffset":
        document.getElementById("roll-roll").yoffset=k.value;
        break;
    }
}

class pianoRoll {
    placePianoRoll(div, id) {
        div.html(`<div id='base'><webaudio-pianoroll width="800" height="320"
		timebase="16" loop="1"
		xrange="64"
		yrange="22"
		markend="960"
		grid="16"
		wheelzoom="1" id=\"` + id + `-roll\" editmode=\"dragpoly\"></webaudio-pianoroll></div>`)
    }

    writeRoll([trackId, rollId, obj], rollElementId) {
        function westernToNoteNumber(noteName){
            // To convert western to note numbers, simply find the name of the note, it's offset from the octave, and our octave.

            let nameAndOctave = noteName.match(/(.+?)(\d+)/)

            let chromaticsFromC = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B", "B#"]

            let noteNumber = chromaticsFromC.indexOf(nameAndOctave[1]) + nameAndOctave[2]*12
            //console.log(noteNumber)

            return noteNumber

        }

        function standardToPianoRoll(name, start, end) {
            let object = {
                n: westernToNoteNumber(name),
                t: start,
                g: end-start
            }

            return object
        }

        let roll = obj.getRoll(trackId, rollId)
        console.log(roll, trackId, rollId)
        let pianoRollNotes = []

        for (const note of roll){
            pianoRollNotes.push(standardToPianoRoll(note.note, note.start, note.end))
        }

        $("#" + rollElementId)[0].sequence = pianoRollNotes
    }
    
    saveRoll([trackID, rollID, obj], noteEvents) {
        function noteNumberToWestern(number) {
            // notenumbers appear to come from C0 and then expand up from that.
            // To convert notenumbers into standard western notation, we can simply find the octave by flooring division by 12 and finding the floor of the result, minus one (to offset the octave). To find the note itself, we can simply modulus the notenumber by 12. This returns the number of semitones above c of that octave.
    
            let octave = Math.floor(number/12)-1
            let stepsFromC = number%12
    
            // We can covert the steps from c into a note name by using this list:
    
            let chromaticsFromC = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B", "B#"]
    
            let noteName = chromaticsFromC[stepsFromC] + "" + octave // String in the middle because i'm too lazy to figure out how adding an int to a string is done in JS.
    
            return noteName
        }
    
        function convertToNotation(noteEvents) {
            let roll = []
    
            // noteEvents is an array containing a series of objects. In these objects we care about three main kinds of data; t, the time to place the note, g, the note length, and n, the note number.
    
            for (const note of noteEvents){
                console.log(note)
                let noteName = noteNumberToWestern(note.n)
    
                roll.push({note: noteName, start: note.t, end: note.t+note.g})
            }
    
            return roll
        }

        console.log(noteEvents)
        let notated = convertToNotation(noteEvents)

        obj.writeRoll(trackID, rollID, notated)

        console.log(notated)

        return notated
    }

    constructor([trackId, rollId, obj], parent) {
        let id = "roll"
        $("#main").append($("<div>").attr("id", id))

        console.log("Starting piano roll")

        this.placePianoRoll(parent, "roll")

        let roll = $("#" + id + "-roll")

        this.writeRoll([trackId, rollId, obj], id + "-roll")

        let saveRoll = this.saveRoll
        let synth = obj.giveContext(trackId)

        const player = new synthPlayer(synth)

        let play = $("<button>").text("play").click(function() {player.play(saveRoll([trackId, rollId, obj], roll[0].sequence))})
        let stop = $("<button>").text("stop").click(function (){player.panic()}) // TODO: Use a more intelligent way to stop all sounds.
        let save = $("<button>").text("save").click(function (){saveRoll([trackId, rollId, obj], roll[0].sequence)})

        let close = $("<button>").text("close").click(function (){parent.remove()})

        parent.append(play, stop, save, close)

        console.log(roll.sequence)

    }
}
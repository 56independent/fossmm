class synthPlayer {
    findStartInSeconds(tickCount, start, bpm) {
        return (start/tickCount)/(bpm/60)
    }

    findLengthInSeconds(tickCount, start, end, bpm){
        return ((end-start)/tickCount)/(bpm/60)
    }

    play(notes){ // Notes are given in our standard form, not pianoroll form
        const now = Tone.now()

        for (const note of notes) {
            this.synth.triggerAttackRelease(note.note, now + this.findLengthInSeconds(2, note.start, note.end, 120),  now + this.findStartInSeconds(2, note.start, 120))
        }
    }

    panic() {
        Tone.Transport.cancel();

        //Tone.Transport.releaseAll();
    }

    constructor (synth){
        this.synth = synth

        document.querySelector('button')?.addEventListener('click', async () => {
            await Tone.start()
        })
    }
}
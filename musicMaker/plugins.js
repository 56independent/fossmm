// Module for the plugin management screen

class plugins {
    publishPlugin() {
        this.currentPlugin = {
            pluginName: $("#name").val() || this.currentPlugin.pluginName,
            description: $("#description").val() || this.currentPlugin.description,
            author: $("#author").val() || this.currentPlugin.author,
            language: $("#pluginLanguage") || this.currentPlugin.language,
            repo: $("#git").val() || this.currentPlugin.repo,
            code: this.editor.getValue()
        }

        console.log(this.mainObj.updatePlugin(this.currentPlugin, this.currentPlugin.pluginName))
        if (! this.mainObj.updatePlugin(this.currentPlugin, this.currentPlugin.pluginName)){
            this.mainObj.addPlugin(this.currentPlugin)
        }

        let parent2 = $("#manager").parent()
        $("#manager").remove()

        this.makeManager(this.mainObj, parent2)
    }

    makeIDE(mainObj, parent, pluginName="") {
        if (! mainObj.getPlugin(pluginName)) {
            pluginName = "Test"
        }

        this.mainObj = mainObj
        this.pluginName = pluginName

        this.currentPlugin = mainObj.getPlugin(pluginName) || {
            pluginName: "Test",
            description: "Tiny plugin",
            author: "",
            language: "",
            repo: "",
            code: "(print \"Hello, World!\")"
        }
        
        let pane = $("<td>").attr("id", "ide")
        let buttonRow = $("<div>").append(
            $("<button>").text("Export plugin").click(() => {
                downloadText(JSON.stringify(this.currentPlugin, null, 4), "plugin " + this.currentPlugin.pluginName + ".json")
            }),
            $("<button>").text("Import plugin").click(() => {
                // ChatGPT generated this code
                openFileDialogAndGetContents().then(contents => {
                    this.currentPlugin = contents // TODO: make importing change the values down there live for the user
                }).catch(error => {
                    console.error(error);
                });
            }),
            $("<button>").text("Rerun").click(() => {
                codeEditor.change()
            }),
            $("<p>").text("No errors yet!").attr("id", "errorMonitor")
        )
        let info = $("<div>").append(
            $("<input type='text'>").attr("id", "name").attr("placeholder", "Plugin name").change(() => {this.publishPlugin()}).val(this.currentPlugin.pluginName),
            $("<input type='text'>").attr("id", "description").attr("placeholder", "Description").change(() => {this.publishPlugin()}).val(this.currentPlugin.description),
            $("<input type='text'>").attr("id", "author").attr("placeholder", "Main author").change(() => {this.publishPlugin()}).val(this.currentPlugin.author),
            $("<input type='text'>").attr("id", "git").attr("placeholder", "Git repo").change(() => {this.publishPlugin()}).val(this.currentPlugin.repo),

            $("<select>").attr("id", "pluginLanguage").append(
                $("<select>").text("JS").attr("value", "JS"),
                $("<select>").text("LISP-Plot").attr("value", "LISP-Plot"),
                $("<select>").text("RPN Plot").attr("value", "RPN Plot")
            ).change(() => {this.publishPlugin()})
        )

        let codeEditor = $("<div>").attr("id", "editor").text(this.currentPlugin.code).css("height", "50vh")

        pane.append(buttonRow, info, codeEditor)
        parent.append(pane)

        this.editor = ace.edit("editor")
        this.editor.setTheme("ace/theme/monokai")
        this.editor.session.setMode("ace/mode/lisp")
        this.editor.setFontSize(20)

        codeEditor.change(() => {
            this.publishPlugin()

            console.log(this.editor.getValue())
            //try {
                var plotInterpreterObject = new plotInterpreter(this.editor.getValue(), $("#pluginLanguage").val() || "plisp") // TODO: Handle different languages
                $("#errorMonitor").text(plotInterpreterObject.innards) // TODO: Make error showing work by editing the plot interpreter
            // } catch (e) {
            //     $("#errorMonitor").text(e.msg)
            // }
        })
    }

    makeManager(mainObj, parent) {
        let pane = $("<td>").attr("id", "manager")

        for (const plugin of mainObj.project.settings.plugins) {
            console.log(plugin)
            pane.append(
                $("<tr>").append(
                    $("<h3>").text(plugin.pluginName),
                    $("<p>").text(plugin.description),
                    $("<sub>").text(plugin.author)
                ).click(() => {
                    let parent2 = $("#ide").parent()
                    $("#ide").remove()
                    this.makeIDE(mainObj, parent2, plugin.pluginName)
                })
            )
        }

        parent.append(pane)
    }

    constructor(parent, mainObj) {
        let parent2 = $("<tr>")
        parent.append($("<table>").append(parent2))

        this.makeIDE(mainObj, parent2)
        this.makeManager(mainObj, parent2)
    }
}
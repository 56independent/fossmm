class pageLayout {
    constructor (){
        let grid = $("<table>").attr("id", "globalGrid")
        let row = grid.append("<tr>")
        $("#main").append(grid)
        this.globalGrid = grid
    }

    addSplit(direction, id) {
        let element;
    
        switch (direction) {
            case "up":
                element = $("<tr>").prependTo(this.globalGrid);
                break;
            case "down":
                element = $("<tr>").appendTo(this.globalGrid);
                break;
            case "left":
                element = $("<td>").prependTo(this.globalGrid.find("tr:first"));
                break;
            case "right":
                element = $("<td>").appendTo(this.globalGrid.find("tr:first"));
                break;
        }
    
        element.attr("id", id);
    
        return element;
    }
    
}
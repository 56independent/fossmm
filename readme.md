## Introduction
**Note** This program is so far incomplete and not safe for regular use

This program, called "FOSS music maker" for the time-being is a program designed to give EDM and other music-making genres back to the people. It aims to be:

* Simple to conceptualise and understand
* Capable of a lot, ideally more then propietary DAWs

The final goal of the app is:


## Access/Installation
This application was written in a web-compatible standard to ensure portability. There is no need to install the software; simply visiting the website and accessing it will be enough to use the application.

Installation, if deemed appropiate, can be done by simply downloading from the repository and opening `index.html` found within.

## Programming
This program is designed in a modular way, using classes to help isolate functionality and the various components of the software.

Each file contains a class responsible for the functionality of the program, save for `utils.js`, which contains an assortment of functions. This type of file, whilst a symptom of bad design, provides enough short-term practicality to be useful in the current time.

The creator has chosen the iterative approach to software design; this means each module has its own seperate development lifecycle with its own small stages of design, coding, and testing.

## Plugins (not implemented yet)
Plugins can be programmed in three different languages, Plot, RPN Plot, and JS. Plot is a little like LISP, except for the fact that "function chaining" is suported, where arguments move from right to left. RPN plot is like plot, save for the fact it's in Reverse Polish Notation, thus has the function names after the arguments.

Here is quick comparison:

```
// JS:

console.log("Hello, World!")

console.log("Hello," + " World!")

// Plot:

(print "Hello, World!")

(print concat "Hello" " World!")

or

(print (concat "Hello" " World!"))

// RPN Plot

"Hello, World!" print

"Hello," " World!" concat print
```
